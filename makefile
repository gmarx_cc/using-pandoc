.PHONY: all 


%.pdf: %.md
	pandoc \
	--filter pandoc-crossref \
	--bibliography=references.bib \
	-s $< -o $@


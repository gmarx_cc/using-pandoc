# Readme

This repository contains an example of using pandoc with pandoc-citeproc.
Thus, it is neccessary to install pandoc and the pandoc-citeproc. You can use a package manager like homebrew (mac and linux), or chocolatey for windows, or apt-get for linux. Most of the students use windows, thus I will detail hot to install chocolatey (choco) in detail. 

You may need the "powershell" (PS) installed on your system, on windows 7, 8, 8.1 and 10 it is already installed. Then, run powershell as administrator, otherwise you can't install anything from PS, and paste the following command into the PS promt:

``` 
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
```


Then, quit the PS and run it again. Now try to test choco by executing:

```
PS> choco -v
```
and the answer shoulb be something like `0.10.17` 

Now it is possible to install apps like git, or pandoc and pandoc-citeproc (python 3). You can try it with **pandoc**: 

```
>choco install pandoc
Chocolatey v0.10.15
Installing the following packages:
pandoc
By installing you accept licenses for the ....
```

After installing an application it is necessary to restart again powerhell; don't forget to run it again as  administrator. 

Once you have **pandoc** installed. Let's install the pacjages required to make citation on documents.

```
PS> choco install rsvg-convert python -y
```

Now lets try to compile our demo document content here by executing the next command on the folder's repository:

```
PS> pandoc --filter pandoc-citeproc --bibliography=references.bib -s main.md -o paper.pdf
```

# Some useful option on title:

```
---
  title: "Monitoreo y análisis de variables en procesos electro-químicos para la estimación de cinética química y mecanismo de reacción utilizando sistemas embebidos con IoT"
  date: "Enero-2019"
  author:
    - M.C. Gerardo Marx Chávez-Campos,
    - Dr. Enrique Reyes-Archundia, and
    - Dr. Juan Alfonso Salazar Torres
  institute: Instituto Tecnológico de Morelia
  documentclass: itmprotocol
  classoption:
  bibliography: referenciasProtocoloPhD.bib
  csl: ieee.csl
  link-citations: true
  numbersections: true
  eqnos-eqref: true
  eqnos-cleveref: true
  eqnos-plus-name: Eq.
  fignos-plus-name: Fig.
  fignos-cleveref: true
---
```


Gerardo Marx 
18/feb/2020 
